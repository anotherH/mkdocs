
#Como Configurar

##Manera 1

Es crear un fichero html llamado footer.html y editarlo como se quiere.

añadir al mkdocs.yml como:

`theme_dir: docs/theme`

El theme_dir lo que hace es coger cualquier fichero html que existe en esta carpeta y substituir lo con lo de la carpeta theme de cuando se hace un build. 

 [Tutorial oficial](http://mkdocs.readthedocs.io/en/latest/user-guide/styling-your-docs/)

##Manera 2
Al **mkdocs.yml** se tiene que poner un copyright.

Ejemplo de mkdocs.yml: 

`site_name: M08 Prueba
dev_addr: 0.0.0.0:8000
pages:
- Home: index.md
- About: about.md
- Importante:
    - 'Pagina1': 'pagina1.md'
    - 'Pagina2': 'pagina2.md'
- Pagina3: pagina3.md
- Como hacer el Footer: pagina4.md
- Pagina5: pagina5.md
- Contact: contact.md
theme: readthedocs
copyright: daw iam 2016`


